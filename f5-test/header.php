<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!--[if IE]><script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script><![endif]-->

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Template Basic Images Start -->
	<meta property="og:image" content="path/to/image.jpg">
	
	<!-- SITE FAVICON -->
	<?php wp_site_icon(); ?>
	<!-- END SITE FAVICON -->
	<?php wp_head(); ?>

</head>

<body id="welcome" data-spy="scroll" data-target="#navbarTogglerDemo03" data-offset="160">

	<!-- HEADER -->
	<header class="header fixed-top">
		<div class="header-top py-3">
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-6 col-lg-3 text-center text-sm-left mb-0">
						<a href="<?php echo home_url(); ?>" class="logo"><img src="<?php bloginfo('template_directory'); ?>/assets/img/logo.png" alt="logo"></a>
					</div>
					<div class="col-12 col-sm-6 col-md text-center d-none d-sm-flex align-items-center justify-content-center justify-content-sm-end">
						<div class="header-contacts d-flex flex-column flex-lg-row align-items-end align-items-lg-center">
							<div class="header-contacts__phone d-flex align-items-center align-self-center align-self-sm-auto mb-1 mb-lg-0 mr-lg-5">
								<a href="tel:555555"><i class="fas fa-phone pr-1"></i>call us</a><i class="mx-1">&#10072;</i>
								<span>55 55 55 55</span>
							</div>
							<div class="header-contacts__email d-flex align-items-center">
								<a href="mailto:test@gmail.com"><i class="fas fa-envelope pr-1"></i>email us</a><i class="mx-1">&#10072;</i>
								<span>test@gmail.com</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="header-bottom bg-blue">
			<div class="container">
				<div class="row justify-content-between align-items-center py-3 py-lg-0">
					<div class="col-6 d-lg-none">
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
							<i class="fas fa-bars"></i>
						</button>
					</div>			
					<div class="col-6 col-sm-4 col-lg-2 order-lg-1 align-self-stretch">
						<a href="#form" class="btn btn-to-form">get a quote</a>
					</div>
					<div class="col-12 col-lg-auto order-lg-0">
						<nav class="navbar navbar-expand-lg p-0">			
							<?php wp_nav_menu(array(
								'theme_location'  => 'top',
								'menu'            => '', 
								'container'       => 'div', 
								'container_class' => 'collapse navbar-collapse', 
								'container_id' => 'navbarTogglerDemo03', 
								'menu_class'      => 'navbar-nav mr-auto mt-3 mt-lg-0 ml-0', 
							)); ?>
						</nav>
					</div>
				</div>
			</div>
		</div>
    </header>
    <!-- END HEADER -->