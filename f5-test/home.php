<?php
/*
Template Name: Home page
Template Post Type: page
 */
?>

<?php get_header(); ?>

    <?php get_template_part('template-parts/content', 'home'); ?>

<?php get_footer(); ?>