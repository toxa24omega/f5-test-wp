	<!-- FOOTER -->
	<footer class="footer">
		<div class="container text-center">
			<div class="row">
				<div class="col-12 footer__visit py-4">
					<p>Looking to learn more about our company?</p>
					<a href="#" class="btn mt-3">Visit LDTool.com</a>
				</div>
			</div>
		</div>
		<hr class="w-100 m-0">
		<div class="container text-center">
			<div class="row">
				<div class="col-12 footer__copyright py-4"><p>&copy; L-D Tool & Die, All rights reserved, 2018</p></div>
			</div>
		</div>
	</footer>

	<?php wp_footer(); ?>

</body>

</html>