<?php

    // Add SEO template
    require_once 'seo.php';
    // Add Customizer File
    require_once 'customizer/f5-custom-customizer.php';

    // Actions
    add_action( 'wp_enqueue_scripts', 'style_theme' );
    add_action( 'wp_footer', 'scripts_theme' );
    add_action( 'after_setup_theme', 'theme_register_nav_menu' );
    add_action( 'init', 'create_post_type' );

    // Register new menu position
    function theme_register_nav_menu() {
        register_nav_menu( 'top', 'Меню в шапке' );
    }
    // Add styles
    function style_theme() {
        wp_enqueue_style( 'style', get_stylesheet_uri() );
        wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/css/main.css' );
    }
    // Add scritps
    function scripts_theme() {
        wp_enqueue_script( 'scripts.min', get_template_directory_uri() . '/assets/js/scripts.min.js');
    }

    // Create new post_type in admin panel
    function create_post_type() {
        register_post_type( 'review',
            array(
                'labels'        => array(
                'name'          => 'Отзывы',
                'singular_name' => 'Отзывы',
                'add_new'       => 'Добавить отзыв',
                'add_new_item'  => 'Добавление отзыва',
                'menu_icon'     => 'dashicons-format-quote',
                'view_item'     => 'Смотреть отзыв',
            ),
            'public' => true,
            'has_archive' => false,
            )
        );
    }

    //Filter
    add_filter( 'nav_menu_css_class', 'filter_nav_menu_css_classes', 10, 4 );
    add_filter( 'nav_menu_link_attributes', 'filter_nav_menu_link_attributes', 10, 4 );

    function filter_nav_menu_css_classes( $classes, $item, $args, $depth ) {
        if( $args->theme_location === 'top' ){
            $classes = [
                'nav-item'
            ];
        }
        return $classes;
    }
    function filter_nav_menu_link_attributes( $atts, $item, $args, $depth ) {
        if( $args->theme_location === 'top' ){
            $atts['class'] = 'nav-link';
        }
        return $atts;
    }
    
    // ADD THUMBNAILS THEME SUPPORT 
    add_theme_support('post-thumbnails');
?>