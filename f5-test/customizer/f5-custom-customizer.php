<?php

    add_action( 'customize_register', 'my_customize_register' );

    function my_customize_register( $wp_customize ) {

        $wp_customize->add_section( 'my_custom_customize_section', array(
            'title'         => __( 'Баннер', 'omega' ),
            'description'   => esc_html__( 'Редактор баннера. Здесь можно задать логотип, а также заголовок.', 'omega' ),
        ));

        // TITLE INPUT
        $wp_customize->add_setting('my_custom_customize_title_setting', array(
            'default' => '', // Optional.
        ));

        $wp_customize->add_control('my_custom_customize_control_title', array(
            'label'     => __('Заголовок', 'omega'),
            'section'   => 'my_custom_customize_section',
            'settings'  => 'my_custom_customize_title_setting',
            'priority'  => 10,
        ));

        // SUBTITLE INPUT 
        $wp_customize->add_setting('my_custom_customize_setting_subtitle', array(
            'default' => '', // Optional.
        ));

        $wp_customize->add_control('my_custom_customize_control_subtitle', array(
            'label'     => __('Подзаголовок', 'omega'),
            'section'   => 'my_custom_customize_section',
            'settings'  => 'my_custom_customize_setting_subtitle',
            'priority'  => 20,
        ));

        // LOGO IMAGE
        $wp_customize->add_setting( 'my_custom_customize_control_logo',
            array(
                'default' => '',
            )
        );

        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'my_custom_customize_control_logo',
            array(
                'label'         => __( 'Изображение' ),
                'description'   => esc_html__( 'Выбирите изображение' ),
                'section'       => 'my_custom_customize_section',
                'priority'      => 30,
                'button_labels' => array( // Optional.
                    'select'        => __( 'Select Image' ),
                    'change'        => __( 'Change Image' ),
                    'remove'        => __( 'Remove' ),
                    'default'       => __( 'Default' ),
                    'placeholder'   => __( 'No image selected' ),
                    'frame_title'   => __( 'Select Image' ),
                    'frame_button'  => __( 'Choose Image' ),
                )
            )
        ));
    }

?>