<!-- BANNER -->
<section class="banner bg-section" style="background-image: url('<?php bloginfo('template_directory'); ?>/assets/img/banner_bg.jpg');">
    <div class="container d-flex h-100 flex-column">
        <div class="row flex-fill d-flex justify-content-center align-items-center">
            <div class="col-12 col-lg-8 text-center">
                <!-- BANNER IMG -->
                <?php
                
                if ( get_theme_mod( 'my_custom_customize_control_logo' ) ) : ?>
                    
                    <figure><img src="<?php echo get_theme_mod( 'my_custom_customize_control_logo' ) ?>" class="img-responsive m-auto" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" ></figure>
                    
                    <?php
                    else : ?>

                <?php endif; ?>

                <div class="banner-text">
                    <!-- BANNER TITLE -->
                    <?php

                    if (get_theme_mod('my_custom_customize_title_setting')) : ?>
                        <h1><?php echo get_theme_mod( 'my_custom_customize_title_setting' ) ?></h1>
                    <?php else : ?>
                            <h1>quality. speed. value.</h1>
                    <?php endif; ?>

                    <!-- BANNER SUBTITLE -->
                    <?php

                    if (get_theme_mod('my_custom_customize_setting_subtitle')) : ?>
                        <p><?php echo get_theme_mod( 'my_custom_customize_setting_subtitle' ) ?></p>
                    <?php else : ?>
                        <p>Are you satisfied with the quality of parts being sourced directly from China? Bring your manufacturing back to Canada and save up to 20% on the total cost of ownership. Learn more below.</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- PROUDLY -->
<section class="proudly">
    <div class="container">
        <div class="row justify-content-center py-5">
            <div class="col-12 col-lg-10 d-flex flex-column justify-content-center align-items-center text-center">
                <div class="proudly-title"><?php the_field('sertificat-title', 118); ?></div>
                <a href="<?php the_field('sertificat-link', 118); ?>" class="btn btn-more mt-4">Learn more</a>
                <p class="mt-4"><?php the_field('sertificat-text', 118); ?></p>
            </div>
        </div>
    </div>
</section>
<!-- WHY CHOOSE US -->
<section id="choise" class="why-us bg-section bg-overlay py-5" style="background-image: url('<?php bloginfo('template_directory'); ?>/assets/img/why-choise_bg.jpg')">
    <div class="container">
        <div class="row justify-content-center text-center why-us-title">
            <div class="col-12"><h2 class="borders">why choose us?</h2></div>
            <div class="col-12 col-lg-7"><p class="pt-5">"Our job is to ruthlessly eliminate the unknown variables that will cost you time and money." - Laurie Dickson, CEO</p></div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4 d-flex">
                <div class="block mt-5 p-4">
                    <div class="block__title d-flex align-items-center">
                        <div class="icon"><img src="<?php $title = get_field('icon');	
                            if( $title ): ?>
                                <?php echo $title['item1']; ?>
                            <?php endif; ?>" alt="icon">
                        </div>
                        <div class="title ml-2">
                            <?php
                                $title = get_field('title');	
                                if( $title ): ?>
                                    <?php echo $title['item1']; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="block__text mt-3">
                        <p>
                            <?php
                                $main_text = get_field('main_text');	
                                if( $main_text ): ?>
                                    <?php echo $main_text['item1']; ?>
                            <?php endif; ?>    
                        </p>
                        <span class="d-block mt-3">
                            <?php
                                $main_text = get_field('bottom_text');	
                                if( $main_text ): ?>
                                    <?php echo $main_text['item1']; ?>
                            <?php endif; ?>    
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 d-flex">
                <div class="block mt-5 p-4">
                    <div class="block__title d-flex align-items-center">
                        <div class="icon"><img src="<?php $title = get_field('icon');	
                            if( $title ): ?>
                                <?php echo $title['item2']; ?>
                            <?php endif; ?>" alt="icon">
                        </div>
                        <div class="title ml-2">
                            <?php
                                $title = get_field('title');	 
                                if( $title ): ?>
                                    <?php echo $title['item2']; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="block__text mt-3">
                        <p>
                            <?php
                                $main_text = get_field('main_text');	
                                if( $main_text ): ?>
                                    <?php echo $main_text['item2']; ?>
                            <?php endif; ?>    
                        </p>
                        <span class="d-block mt-3">
                            <?php
                                $main_text = get_field('bottom_text');	
                                if( $main_text ): ?>
                                    <?php echo $main_text['item2']; ?>
                            <?php endif; ?>    
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-4 d-flex">
                <div class="block mt-5 p-4">
                    <div class="block__title d-flex align-items-center">
                        <div class="icon"><img src="<?php $title = get_field('icon');	
                            if( $title ): ?>
                                <?php echo $title['item3']; ?>
                            <?php endif; ?>" alt="icon">
                        </div>
                        <div class="title ml-2">
                            <?php
                                $title = get_field('title');	
                                if( $title ): ?>
                                    <?php echo $title['item3']; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="block__text mt-3">
                        <p>
                            <?php
                                $main_text = get_field('main_text');	
                                if( $main_text ): ?>
                                    <?php echo $main_text['item3']; ?>
                            <?php endif; ?>    
                        </p>
                        <span class="d-block mt-3">
                            <?php
                                $main_text = get_field('bottom_text');	
                                if( $main_text ): ?>
                                    <?php echo $main_text['item3']; ?>
                            <?php endif; ?>    
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- DIFFERENCE -->
<!-- DIFFERENCE -->
<section id="difference" class="difference py-5">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-12"><h2 class="borders">the l-d difference</h2></div>
        </div>
        <!-- EFFORTLESS DOCK-TO-STOCK -->
        <div class="row difference-block mt-5">
            <div class="col-12 mb-3 d-flex">
                <h3><?php $effortless = get_field('effortless');	
                    if( $effortless ): ?>
                        <?php echo $effortless['title']; ?>
                    <?php endif; ?>
                </h3>
            </div>
            <div class="col-12 col-md-6 mb-3 mb-md-0">
                <div class="text"><?php $effortless = get_field('effortless');	
                    if( $effortless ): ?>
                        <?php echo $effortless['text']; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <img src="<?php $effortless = get_field('effortless');	
                if( $effortless ): ?>
                    <?php echo $effortless['image']; ?>
                <?php endif; ?>" class="img-responsive" alt="difference_img">
            </div>
        </div>
        <!-- PROCESS -->
        <div class="row difference-block mt-5">
            <div class="col-12 mb-3 d-flex">
                <h3><?php $process = get_field('process');	
                    if( $process ): ?>
                        <?php echo $process['title']; ?>
                    <?php endif; ?>
                </h3>
            </div>
            <div class="col-12 col-md-6">
                <img src="<?php $process = get_field('process');	
                if( $process ): ?>
                    <?php echo $process['image']; ?>
                <?php endif; ?>" class="img-responsive" alt="difference_img">
            </div>
            <div class="col-12 col-md-6 mb-3 mb-md-0">
                <div class="text"><?php $process = get_field('process');	
                    if( $process ): ?>
                        <?php echo $process['text']; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- EXPERIENCE -->
        <div class="row difference-block mt-5">
            <div class="col-12 mb-3 d-flex">
                <h3><?php $experience = get_field('experience');	
                    if( $experience ): ?>
                        <?php echo $experience['title']; ?>
                    <?php endif; ?>
                </h3>
            </div>
            <div class="col-12 col-md-6 mb-3 mb-md-0">
                <div class="text"><?php $experience = get_field('experience');	
                    if( $experience ): ?>
                        <?php echo $experience['text']; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <img src="<?php $experience = get_field('experience');	
                if( $experience ): ?>
                    <?php echo $experience['image']; ?>
                <?php endif; ?>" class="img-responsive" alt="difference_img">
            </div>
        </div>
        <!-- PARTS -->
        <div class="row difference-block mt-5">
            <div class="col-12 mb-3 d-flex">
                <h3><?php $parts = get_field('parts');	
                    if( $parts ): ?>
                        <?php echo $parts['title']; ?>
                    <?php endif; ?>
                </h3>
            </div>
            <div class="col-12 col-md-6">
                <img src="<?php $parts = get_field('parts');	
                if( $parts ): ?>
                    <?php echo $parts['image']; ?>
                <?php endif; ?>" class="img-responsive" alt="difference_img">
            </div>
            <div class="col-12 col-md-6 mb-3 mb-md-0">
                <div class="text"><?php $parts = get_field('parts');	
                    if( $parts ): ?>
                        <?php echo $parts['text']; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- SECONDARY OPERATIONS -->
        <div class="row difference-block mt-5">
            <div class="col-12 mb-3 d-flex">
                <h3><?php $secondary_operations = get_field('secondary_operations');	
                    if( $secondary_operations ): ?>
                        <?php echo $secondary_operations['title']; ?>
                    <?php endif; ?>
                </h3>
            </div>
            <div class="col-12 col-md-6 mb-3 mb-md-0">
                <div class="text"><?php $secondary_operations = get_field('secondary_operations');	
                    if( $secondary_operations ): ?>
                        <?php echo $secondary_operations['text']; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-12 col-md-6">
            <img src="<?php $secondary_operations = get_field('secondary_operations');	
                if( $secondary_operations ): ?>
                    <?php echo $secondary_operations['image']; ?>
                <?php endif; ?>" class="img-responsive" alt="difference_img">
            </div>
        </div>
    </div>
</section>
<!-- OUR CLIENTS REVIEW -->
<section id="reviews" class="review bg-section bg-overlay py-5" style="background-image: url('<?php bloginfo('template_directory'); ?>/assets/img/our-review_bg.jpg')">
    <div class="container">
        <div class="row justify-content-center">
            <h2 class="borders">our clients review</h2>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                <div class="multiple-items">
                    <?php
                    global $post;
                    $args = array( 'numberposts' => -1, 'post_type' => 'review', 'suppress_filters' => true );
                    
                    $myposts = get_posts( $args );
                    foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                        <div>
                            <div class="review-block">
                                <div class="title"><?php the_title(); ?></div>
                                <div class="text"><p><?php the_content(); ?></p></div>
                            </div>
                        </div>
                    <?php endforeach; 
                    wp_reset_postdata();?>
                </div>
                <div class="slick-pagination">
                    <div class="prev"><i class="fas fa-chevron-left"></i></div>
                    <div class="next"><i class="fas fa-chevron-right"></i></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- LEAN SUPPLY CHAIN -->
<section id="supply" class="supply py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="borders mb-4">LEAN SUPPLY CHAIN = HIGHER PROFITS</h2>
            </div>
        </div>
        <div class="supply-news">
            <div class="row">

                <?php
                global $post;
                $args = array( 'numberposts' => -1, 'category_name' => 'profits', 'suppress_filters' => true, 'order' => 'ASC');
                
                $myposts = get_posts( $args );
                foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                    <div class="col-12 col-md-6 col-lg-3 mb-3">
                        <a href="#" class="supply-news__item">
                            <figure>
                                <div class="img" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div>
                                <figcaption><h5><?php the_title(); ?></h5></figcaption>
                            </figure>
                            <hr>
                            <p><?php the_content(); ?></p>
                        </a>
                    </div>
                <?php endforeach; 
                wp_reset_postdata();?>
            </div>
        </div>
    </div>
</section>
<!-- FORM -->
<section id="form" class="form py-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h2 class="borders">READY TO GET STARTED?</h2>
                <p class="my-3">Our ISO certified processes will deliver high quality parts for the lowest cost of ownership, period.<br>Please fill out the form below and someone will get back to you shortly!</p>
            </div>
            <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                <?php echo do_shortcode( '[contact-form-7 id="115" title="Контактная форма"]' ) ?>
            </div>
        </div>
    </div>
</section>