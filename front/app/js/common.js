// SLICK-CAROUSEL
$('.multiple-items').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 2,
    prevArrow: $('.prev'),
    nextArrow: $('.next'),
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

// SCROLL NAV
$('.navbar-nav li a').click(function(e) {				
    e.preventDefault();
    var hrefLink = $(this).attr('href');
    $('body, html').stop(true, true).animate({				
        scrollTop: $(hrefLink).offset().top - 120
    },1000);
});

